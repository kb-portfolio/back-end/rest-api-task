const Products = require('../models/productsM');
const {
    createSchema,
    idSchema,
    updateSchema,
    findAllSchema
} = require('./product.joi')

const findOne = async function (req, res, next) {
    const {
        value,
        error
    } = idSchema.validate({
        _id: req.params.productId,
    })

    if (error) {
        const err = new Error("Get product failed.")
        err.forUser = true
        err.status = 422
        err.errors = error.details.map((e) => e.message)
        throw err
    }

    const product = await Products.findById(value._id, {
        where: {
            deleted: '0'
        }
    })

    console.log(product)

    if (!product) {
        const err = new Error(`Product not found - _id='${value._id}'.`)
        err.forUser = true
        err.status = 404
        throw err
    }

    res.status(200).json(product)
}

const findAll = async function (req, res, next) {
    let rawFields = req.query.fields;

    if (rawFields) {
        rawFields = rawFields.split(',')
    }

    const {
        value,
        error
    } = findAllSchema.validate({
        name: req.query.name,
        description: req.query.description,
        limit: req.query.limit,
        offset: req.query.offset,
        fields: rawFields,
    }, {
        abortEarly: false
    })
    if (error) {
        const err = new Error("Find all products failed.")
        err.forUser = true
        err.status = 422
        err.errors = error.details.map((e) => e.message)
        throw err
    }

    let {
        fields,
        limit,
        offset,
        name,
        description,
    } = value

    // let attributes = []
    // if (fields) attributes = fields

    // limit = Math.min(Math.max(0, +limit), 25)
    // offset = Math.min(Math.max(0, +offset), 1000)

    const whereObj = {
        deleted: '0'
    }
    if (name) whereObj.name = new RegExp(name, 'i')
    if (description) whereObj.description = new RegExp(description, 'i')

    const products = await Products.findAll({
        where: whereObj,
        attributes: fields,
        limit: limit || 0,
        offset: offset || 0
    })
    res.status(200).json(products)
}

const create = async function (req, res, next) {
    const {
        value,
        error
    } = createSchema.validate({
        name: req.body.name,
        imgSrc: req.body.imgSrc,
        description: req.body.description
    }, {
        abortEarly: false
    })

    if (error) {
        const err = new Error("Create product failed.")
        err.forUser = true
        err.status = 422
        err.errors = error.details.map((e) => e.message)
        throw err
    }

    const products = await Products.insertOne({
        ...value
    })

    res.status(201).json({
        message: "Product successfully created.",
        _id: products._id
    })
}

const update = async function (req, res, next) {
    const {
        value,
        error
    } = updateSchema.validate({
        name: req.body.name,
        imgSrc: req.body.imgSrc,
        description: req.body.description,
        _id: req.params.productId
    }, {
        abortEarly: false
    })

    if (error) {
        const err = new Error("Update product failed.")
        err.forUser = true
        err.status = 422
        err.errors = error.details.map((e) => e.message)
        throw err
    }

    const updateResult = await Products.updateOne(value._id, {
        name: value.name,
        imgSrc: value.imgSrc,
        description: value.imgSrc
    })

    if (!updateResult.result.n) {
        const err = new Error(`Product not found - _id='${value._id}'.`)
        err.forUser = true
        err.status = 404
        throw err
    }

    res.status(200).json({
        message: "Product successfully updated.",
    })
}

const remove = async function (req, res, next) {
    const {
        value,
        error
    } = idSchema.validate({
        _id: req.params.productId,
    })

    if (error) {
        const err = new Error("Get product failed.")
        err.forUser = true
        err.status = 422
        err.errors = error.details.map((e) => e.message)
        throw err
    }

    const removeResul = await Products.remove(value._id)

    if (!removeResul.result.n) {
        const err = new Error(`Product not found - _id='${value._id}'.`)
        err.forUser = true
        err.status = 404
        throw err
    }
    res.status(200).json({
        message: "Product successfully removed."
    })
}

module.exports = {
    findOne,
    findAll,
    create,
    update,
    remove,
}