const Joi = require('@hapi/joi');

const schema = Joi.object({
    name: Joi.string()
        .min(3)
        .max(60),
    imgSrc: Joi.string()
        .uri(),
    description: Joi.string()
        .min(10)
        .max(1000)
})

const idSchema = Joi.object({
    _id: Joi.string()
        .hex()
        .length(24)
        .required()
})

const updateSchema = schema.keys({
    _id: Joi.string()
        .alphanum()
        .length(24)
        .required()
})

const findAllSchema = schema.keys({
    limit: Joi.number()
        .allow('')
        .min(0)
        .max(25),
    offset: Joi.number()
        .allow('')
        .min(0)
        .max(100),
    fields: Joi.array()
        .items(Joi.string().valid("name", "imgSrc", "description"))
        .allow(''),
    name: Joi.optional(),
    description: Joi.optional(),
    imgSrc: null
})

const createSchema = schema


module.exports = {
    schema,
    createSchema,
    idSchema,
    updateSchema,
    findAllSchema
}