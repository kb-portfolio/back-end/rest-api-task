const mongodb = require('mongodb');
const getDb = require('../util/database').getDb;

const ObjectId = mongodb.ObjectID;

class Products {

    static insertOne(data) {

        const db = getDb();
        return db.collection('products').insertOne({
            created_at: new Date(),
            deleted: '0',
            ...data
        })
    }

    static findById(id, obj = {}) {
        const {
            where
        } = obj

        if (!id) {
            throw new Error('Id canot be empty');
        }
        const db = getDb();
        return db.collection('products').findOne({
            _id: new ObjectId(id),
            ...where
        })
    }

    static findAll(obj = {}) {
        const {
            where,
            attributes,
            limit,
            offset
        } = obj

        const project = {}
        if (attributes) {
            attributes.forEach((attribute) => {
                project[attribute] = 1
            })
        }

        const db = getDb();
        return db.collection('products').find({
                ...where
            }).project(
                project
            )
            .sort({
                created_at: 1
            })
            .limit(limit || 0)
            .skip(offset || 0)
            .toArray()
    }

    static updateOne(id, data) {
        const {
            name,
            imgSrc,
            description
        } = data
        if (!id) {
            throw new Error('Id canot be empty')
        }
        const db = getDb();
        return db.collection('products').updateOne({
            _id: new ObjectId(id)
        }, {
            $set: {
                updated_at: new Date(),
                name,
                imgSrc,
                description
            }
        })
    }

    static remove(id) {
        if (!id) {
            throw new Error('Id canot be empty');
        }
        const db = getDb();
        return db.collection('products').updateOne({
            _id: new ObjectId(id)
        }, {
            $set: {
                updated_at: new Date(),
                deleted: '1'
            }
        })
    }
}

module.exports = Products;