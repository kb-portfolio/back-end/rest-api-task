db.runCommand({
    collMod: "products",
    validator: {
        $jsonSchema: {
            bsonType: "object",
            required: ["name", 'imgSrc', 'description', 'deleted'],
            properties: {
                name: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },
                imgSrc: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },
                description: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },
                deleted: {
                    enum: ["1", '0'],
                    description: "can only be one of the enum values and is required"
                }
            }
        }
    }
})