const {
    Router
} = require('express')
const {
    catchAsync
} = require('../middlewares/errors');

const productsC = require('../controllers/productsC')

const router = Router()

// GET /products/:id
router.get('/:productId', catchAsync(productsC.findOne))

// GET /products
router.get('', catchAsync(productsC.findAll))

// POST /products
router.post('', catchAsync(productsC.create))

// PUT /products/:id
router.put('/:productId', catchAsync(productsC.update))

// DELETE /products/:id
router.delete('/:productId', catchAsync(productsC.remove))

module.exports = router