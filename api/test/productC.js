const expect = require('chai').expect
const sinon = require('sinon')

const productM = require('../models/productsM')
const productC = require('../controllers/productsC')

describe('Product Controller', function () {
    describe('findOne', function () {
        it('should return true', function () {
            expect(true).to.equal(true)
        })

        it('should throw an error if accessing the database fails', async () => {
            sinon.stub(productM, 'findById');
            productM.findById.throws()

            const req = {
                params: {
                    productId: '5e6fb84eed1c82125cc981f6'
                }
            }

            try {
                const result = await productC.findOne(req, {}, () => {})
            } catch (err) {
                expect(err).to.be.an('error')
            }
            productM.findById.restore()
        })

        it('should throw error if productId not passed', async () => {
            const req = {
                params: {
                    productId: null
                }
            }

            try {
                const result = await productC.findOne(req, {}, () => {})
            } catch (err) {
                expect(err).to.be.an('error')
                expect(err).to.have.property('message', 'Get product failed.')
                expect(err).to.have.property('forUser', true)
                expect(err).to.have.property('status', 422)
            }
        })

        it('should throw error if productId is empty', async () => {
            const req = {
                params: {
                    productId: ''
                }
            }

            try {
                const result = await productC.findOne(req, {}, () => {})
            } catch (err) {
                expect(err).to.be.an('error')
                expect(err).to.have.property('message', 'Get product failed.')
                expect(err).to.have.property('forUser', true)
                expect(err).to.have.property('status', 422)
            }
        })

        it('should throw error if productId is to short', async () => {
            const req = {
                params: {
                    productId: '231dsa2d'
                }
            }

            try {
                const result = await productC.findOne(req, {}, () => {})
            } catch (err) {
                expect(err).to.be.an('error')
                expect(err).to.have.property('message', 'Get product failed.')
                expect(err).to.have.property('forUser', true)
                expect(err).to.have.property('status', 422)
            }
        })

        it('should throw error if productId is to long', async () => {
            const req = {
                params: {
                    productId: '5e6fb84eed1c82125cc981f62'
                }
            }

            try {
                const result = await productC.findOne(req, {}, () => {})
            } catch (err) {
                expect(err).to.be.an('error')
                expect(err).to.have.property('message', 'Get product failed.')
                expect(err).to.have.property('forUser', true)
                expect(err).to.have.property('status', 422)
            }
        })

        it('should throw error if productId is not hex', async () => {
            const req = {
                params: {
                    productId: '1234567890qwertyuiopasdf'
                }
            }

            try {
                const result = await productC.findOne(req, {}, () => {})
            } catch (err) {
                expect(err).to.be.an('error')
                expect(err).to.have.property('message', 'Get product failed.')
                expect(err).to.have.property('forUser', true)
                expect(err).to.have.property('status', 422)
            }
        })

        it('should throw error if product not found', async () => {
            const findById = sinon.stub(productM, 'findById')

            const req = {
                params: {
                    productId: '123456789012345678901234'
                }
            }

            findById.onFirstCall().returns(null);

            try {
                const result = await productC.findOne(req, {}, () => {})
            } catch (err) {
                expect(err).to.be.an('error')
                expect(err).to.have.property('message', `Product not found - _id='${req.params.productId}'.`)
                expect(err).to.have.property('forUser', true)
                expect(err).to.have.property('status', 404)
            }
            productM.findById.restore()
        })
    })
})