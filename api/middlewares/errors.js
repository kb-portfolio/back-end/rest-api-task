const catchAsync = (fn) => {
    return (req, res, next) => {
        fn(req, res, next)
            .catch(err => {
                next(err)
            })
    }
}

const catchErrors = function (err, req, res, next) {
    res.status(err.status || 500)
    if (err.forUser) {
        res.json({
            message: err.message,
            ...err
        })
    } else {
        console.error("APP - Error ", err)
        res.json({
            message: "An error has occurred",
        })
    }
}

module.exports = {
    catchAsync,
    catchErrors,
}