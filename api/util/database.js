const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

let _db;

const mongoDb = `mongodb+srv://application_user:yewWMy8de8OYp4Ny@rest-api-task-cluster-pzo7h.mongodb.net/rest-api-task?retryWrites=true&w=majority`

const mongoConnect = (callback) => {
    MongoClient.connect(mongoDb, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        .then(client => {
            console.log('Connected to database!')
            _db = client.db()
            callback()
        })
        .catch(err => {
            console.log("Failed to connect to database", err)
            throw err
        })
};

const getDb = () => {
    if (_db) {
        return _db
    }
    throw 'No database found'
};
module.exports = {
    mongoConnect,
    getDb
}