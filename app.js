const path = require('path')
const express = require('express')
const bodyParser = require('body-parser')
const expressLayouts = require('express-ejs-layouts')
const {
    catchErrors: appCatchErrors
} = require('./app/middlewares/errors')

const {
    catchErrors: apiCatchErrors
} = require('./api/middlewares/errors')

const appPort = 10301
const apiPort = 10302

// ############### App ###############

const app = express()

app.set('view engine', 'ejs')
app.set('views', 'app/views')
app.set("layout", "layouts/default.ejs")

app.use(expressLayouts);
app.use(express.static(path.join(__dirname, 'app/public')))

// ##### App Routes #####
app.use('/', require('./app/routes/indexR'))

app.use(appCatchErrors)

app.listen(appPort, () => console.log(`App starting on port ${appPort}!`))

// ############### Api ###############

const api = express()

api.use(bodyParser.urlencoded({
    extended: false
}))
api.use(bodyParser.json())

// ##### DB #####

const mongoConnect = require('./api/util/database').mongoConnect
const getDb = require('./api/util/database').getDb

// ##### Api Headers #####

api.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
        'Access-Control-Allow-Methods',
        'OPTIONS, GET, POST, PUT, PATCH, DELETE'
    );
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    }
    next();
});

// ##### Api Routes #####

api.use('/api/v1/products', require('./api/routes/productsR'))

api.use(apiCatchErrors)

mongoConnect(() => {
    api.listen(apiPort, () => console.log(`Api starting on port ${apiPort}!`))
})