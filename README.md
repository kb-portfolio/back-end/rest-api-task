# REST API TASK
This is a simple REST API application

## Installation

1) Make sure you have installed  npm
2) Clone or download repository
3) Run `npm install`;

### After installation

For start application:

1) run `npm run start:dev` for starts application in developers mode (using nodemon)
1) run `npm start` for starts application in production mode 
1) run `npm test` for start test 

## About application

The application is divided into two parts App and API both use node.js and express.

### App

The App uses port 10301 and is responsible for controls which use the API

### Api

The Api uses port 10302 and provide few common function, like find one (GET), find all (GET), create one (POST), update one (PUT) and delete one (DELETE).
1) Find One return one product by id
2) Find all return all products which matching filters and settings. It supports:
    - finding by name and description;
    - filters the returned fields;
    - set limit and offset;
3) Create new product
4) Update whole  product
5) Delte product by id (soft deling)

Api is connect to thhe mongo database in cloud

### Testing

I implemented basic unit tests of product controller methods using mocha and chai