const {
    Router
} = require('express')

const indexController = require('../controllers/indexC')
const {
    catchAsync
} = require('../middlewares/errors')

const router = Router()

router.get('/', catchAsync(indexController.getIndex))

module.exports = router