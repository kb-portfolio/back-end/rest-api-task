const catchErrors = function (err, req, res, next) {
    res.status(err.status || 500)
    if (err.status === 404) {
        res.render('errors/404', {
            message: '404 - Page not found'
        });
    } else {
        res.render('errors/500', {
            message: '500 - Server error'
        });
    }
}

const catchAsync = (fn) => {
    return (req, res, next) => {
        fn(req, res, next)
            .catch(err => {
                console.error('APP - Error ', err.message)
                next(err)
            })
    }
}

module.exports = {
    catchErrors,
    catchAsync,
}