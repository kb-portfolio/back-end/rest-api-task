document.addEventListener("DOMContentLoaded", function (event) {

    const getProductsBtn = document.querySelector(".get-products-btn")
    getProductsBtn.addEventListener('click', async (e) => {
        const inputs = e.target.closest('.get-products').querySelector('.inputs')
        const inputFields = inputs.querySelector('.fields')
        const inputLimit = inputs.querySelector('.limit')
        const inputOffset = inputs.querySelector('.offset')

        const inputName = inputs.querySelector('.name')
        const inputDescription = inputs.querySelector('.description')

        try {
            const products = await core.get(`/api/v1/products`, {
                fields: inputFields.value,
                limit: inputLimit.value,
                offset: inputOffset.value,
                name: inputName.value,
                description: inputDescription.value,
            })

            let docFrag = document.createDocumentFragment()
            products.forEach(product => {
                docFrag.appendChild(core.createProductTile(product))
            });

            const productsWrapper = document.querySelector(".products-wrapper")
            productsWrapper.innerHTML = ''
            productsWrapper.appendChild(docFrag)
        } catch (err) {
            document.querySelector(".products-wrapper").innerHTML = ''
            if (err.json && err.json.errors) {
                err.json.errors.forEach((errMessage) => {
                    err.message += '\n' + errMessage
                })
            }
            alert(err.message)
        }
    })

    const getProductBtn = document.querySelector(".get-product-btn")
    getProductBtn.addEventListener('click', async (e) => {
        const inputs = e.target.closest('.get-product-tile').querySelector('.inputs')
        const input_id = inputs.querySelector('._id')

        try {
            if (!input_id.value) throw new Error("_id is required")
            const product = await core.get(`/api/v1/products/${input_id.value}`)

            const productsWrapper = document.querySelector(".products-wrapper")
            productsWrapper.innerHTML = ''
            productsWrapper.appendChild(core.createProductTile(product))

        } catch (err) {
            document.querySelector(".products-wrapper").innerHTML = ''
            if (err.json && err.json.errors) {
                err.json.errors.forEach((errMessage) => {
                    err.message += '\n' + errMessage
                })
            }
            alert(err.message)
        }
    })

    const createProductBtn = document.querySelector(".create-product-btn")
    createProductBtn.addEventListener('click', async (e) => {
        const inputs = e.target.closest('.create-product').querySelector('.inputs')
        const inputName = inputs.querySelector('.name')
        const inputImgSrc = inputs.querySelector('.imgSrc')
        const inputDescription = inputs.querySelector('.description')

        try {
            const createResponse = await core.post(`/api/v1/products`, {
                name: inputName.value,
                imgSrc: inputImgSrc.value,
                description: inputDescription.value,
            })

            inputName.value = ''
            inputImgSrc.value = ''
            inputDescription.value = ''

            alert(createResponse.message)
        } catch (err) {
            if (err.json && err.json.errors) {
                err.json.errors.forEach((errMessage) => {
                    err.message += '\n' + errMessage
                })
            }
            alert(err.message)
        }
    })

    const updateProductBtn = document.querySelector(".update-product-btn")
    updateProductBtn.addEventListener('click', async (e) => {
        const inputs = e.target.closest('.update-product').querySelector('.inputs')

        const input_id = inputs.querySelector('._id')
        const inputName = inputs.querySelector('.name')
        const inputImgSrc = inputs.querySelector('.imgSrc')
        const inputDescription = inputs.querySelector('.description')

        try {
            if (!input_id.value) throw new Error("_id is required")
            const updateResponse = await core.put(`/api/v1/products/${input_id.value}`, {
                name: inputName.value,
                imgSrc: inputImgSrc.value,
                description: inputDescription.value,
            })

            // input_id.value = ''
            // inputName.value = ''
            // inputImgSrc.value = ''
            // inputDescription.value = ''

            alert(updateResponse.message)
        } catch (err) {
            if (err.json && err.json.errors) {
                err.json.errors.forEach((errMessage) => {
                    err.message += '\n' + errMessage
                })
            }
            alert(err.message)
        }
    })

    const productsWrapper = document.querySelector(".products-wrapper")
    productsWrapper.addEventListener('click', async (e) => {
        if (e.target && e.target.matches('.delete-btn')) {
            const productId = e.target.getAttribute('data-id')
            try {
                const productsResponse = await core.delete(`/api/v1/products/${productId}`)
                e.target.closest('.tile.product').remove()
                alert(productsResponse.message)
            } catch (err) {
                if (err.json && err.json.errors) {
                    err.json.errors.forEach((errMessage) => {
                        err.message += '\n' + errMessage
                    })
                }
                alert(err.message)
            }
        }
    })
});

let apiHost = '';

(function () {
    if (window.location.hostname === 'localhost') {
        apiHost = 'http://localhost:10302'
    }
})()

const core = {
    createProductTile: function (product) {
        let {
            name,
            imgSrc,
            description,
            _id
        } = product

        name = name || ''
        imgSrc = imgSrc || '/img/img-placeholder.png'
        description = description || ''

        const section = document.createElement('article')
        section.className = 'tile product'
        section.innerHTML =
            `<div class="tile-header-container">
                    <h3 class="tile-header">${name}</h3>
                </div>
                <div class="tile-img-container">
                    <img class="tile-img" src="${imgSrc}" alt="">
                </div>
                <div class="tile-description">${description}</div>
                <div class="tile-buttons">
                    <button class="btn delete-btn" type="button" data-id="${_id}">Delete</button>
                </div>
                <div>
                    <span>_id for update </span>
                    <span>${_id}</span>
                </div>`
        return section
    },
    get: async function (url = '', params = {}) {
        const urlWithParams = new URL(apiHost + url);
        if (params) {
            Object.keys(params).forEach(key => urlWithParams.searchParams.append(key, params[key]))
        }

        const response = await fetch(urlWithParams, {
            method: 'GET',
            cache: 'default',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
        });

        if (!response.ok) {
            const jsonResponse = await response.json()
            const err = new Error(jsonResponse.message || "An error occurred")
            err.json = jsonResponse
            throw err
        }

        return await response.json()
    },
    post: async function (url = '', data = {}, params = {}) {

        const urlWithParams = new URL(apiHost + url);
        if (params) {
            Object.keys(params).forEach(key => urlWithParams.searchParams.append(key, params[key]))
        }

        const response = await fetch(urlWithParams, {
            method: 'POST',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify(data)
        });

        if (!response.ok) {
            const jsonResponse = await response.json()
            const err = new Error(jsonResponse.message || "An error occurred")
            err.json = jsonResponse
            throw err
        }

        return await response.json()
    },
    put: async function (url = '', data = {}, params = {}) {

        const urlWithParams = new URL(apiHost + url);
        if (params) {
            Object.keys(params).forEach(key => urlWithParams.searchParams.append(key, params[key]))
        }

        const response = await fetch(urlWithParams, {
            method: 'PUT',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify(data)
        });

        if (!response.ok) {
            const jsonResponse = await response.json()
            const err = new Error(jsonResponse.message || "An error occurred")
            err.json = jsonResponse
            throw err
        }

        return await response.json()
    },
    delete: async function (url = '', data = {}, params = {}) {

        const urlWithParams = new URL(apiHost + url);
        if (params) {
            Object.keys(params).forEach(key => urlWithParams.searchParams.append(key, params[key]))
        }

        const response = await fetch(urlWithParams, {
            method: 'DELETE',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify(data)
        });

        if (!response.ok) {
            const jsonResponse = await response.json()
            const err = new Error(jsonResponse.message || "An error occurred")
            err.json = jsonResponse
            throw err
        }
        return await response.json()
    }
}